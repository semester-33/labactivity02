package hellopackage;
import java.util.*;
import secondpackage.Utilities;

public class Greeter{
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        Random rand = new Random();
        Utilities u = new Utilities();

        System.out.println("Enter an integer number: ");
        int userInput = reader.nextInt();

        userInput = u.doubleMe(userInput);
    
        System.out.println("The new number is: " + userInput);
    }
}